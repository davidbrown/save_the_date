#include "date/date.h"
#include "date/tz.h"

#include <iostream>

using namespace date;

struct DateTime {
	int year;
	unsigned month;
	unsigned day;
	long int hour;
	long int minute;
	long int second;
};

std::ostream& operator<<(std::ostream& os, DateTime& dt) {
	os << dt.year << "-" << dt.month << "-" << dt.day << " "
	   << dt.hour << ":" << dt.minute << ":" << dt.second;
	return os;
}

void exercise5()
{
	// Convert to a time_point
	{
		DateTime const dt {2019, 01, 29, 19, 30, 46};
	}

	// Convert to a DateTime
	{
		sys_seconds const tp{std::chrono::seconds{1548790246}};
	}
}
