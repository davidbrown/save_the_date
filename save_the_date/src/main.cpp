#include "date/date.h"
#include "date/tz.h"

#include <iostream>

void exercise1();
void exercise2();
void exercise3();
void exercise4();
void exercise5();
void exercise6();

using namespace date;

int main()
{
	auto now = make_zoned("Europe/Stockholm", std::chrono::system_clock::now());
	std::cout << "Save the Date workshop "
	          << format("%A %F %H:%M %Z", now) << "\n";

	std::cout << "*********** Exercise 1 ***********\n";
	exercise1();

	std::cout << "\n\n*********** Exercise 2 ***********\n";
	exercise2();

	std::cout << "\n\n*********** Exercise 3 ***********\n";
	exercise3();

	std::cout << "\n\n*********** Exercise 4 ***********\n";
	exercise4();

	std::cout << "\n\n*********** Exercise 5 ***********\n";
	exercise5();

	std::cout << "\n\n*********** Exercise 6 ***********\n";
	exercise6();
	return 0;
}
