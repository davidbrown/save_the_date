# Save The Date

This is a ready-to-go project for the Save the Date workshop.

## Build Instructions
Run the following sequence of commands to get the repo cloned and built:
```
git clone --recursive https://gitlab.com/davidbrown/save_the_date
cd save_the_date
mkdir build && cd build
cmake ..
make
```

Substitute the last two commands with your preferred method of generating and
building.

## Layout
In the folder save_the_date you will find a separate source file for each
exercise. Simply enter your code in to the `exercise*` function, compile, and
run.


